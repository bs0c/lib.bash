#!/usr/bin/env bash

# BASH_USER_LIB_VERSION=0.02

##################
###   Common   ###
##################

msg()
{
    echo >&2 -e "${1-}"
}

die()
{
    local msg=$1
    local code=${2-1} # default exit status 1
    msg ${RED-}"$msg"${NOFORMAT-}
    exit "$code"
}

setup_colors()
{
    if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
        NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m' BOLD='\033[1m'
    else
        NOFORMAT= RED= GREEN= ORANGE= BLUE= PURPLE= CYAN= YELLOW= BOLD=
    fi
}

check_root()
{
    [[ $(id -u) -eq 0 ]] || die "Need root"
}

# Check for installed app and exit if it doesn't exist.
# If the first argument is "true", the function will give you a choice to skip the error
check_app()
{
    for app in $*; do
        if ! type $app &>/dev/null; then
            list_depend+="$app "
        fi
    done
    [[ -z ${list_depend-} ]] || { 
        msg "To continue, you should install: $list_depend"
        [[ $1 == true ]] || return 1
        while true; do
            read -p "Continue, are you sure? (yes | no): "
            case $REPLY in
                yes|y) break;;
                no|n)  die "exit";;
                *)     msg "Sorry don't understand: $REPLY";;
            esac
        done
    }
    return 0
}

# Get the update from the upstream branch and run the script again.
# Existing changes will be hard reset.
# $1 - branch
get_last_update()
{
    msg "try to get last commit from ${1-}"
    local opt="--quiet"
    git checkout $opt $1
    git fetch $opt
    is_change=$(git diff --stat HEAD...HEAD@{upstream})
    [[ -z $is_change ]] || {
        git reset $opt --hard
        git merge $opt
        source "${BASH_SOURCE[0]}" $@
        msg "stop recursion" && exit
    }
}

# Add message from $TAG to top of file
# $1    - path of source
# ${2-} - begining of filename
add_tag()
{
    if [[ -f $1 ]]; then
        list=$1
    else
        list=$(find $1 -type f -name "${2-}*")
    fi
    for file in $list; do
        if ! grep -q "$TAG" $file; then
            local line=1
            [[ $(head -n 1 "$file") =~ ^'#!/' ]] && line=2
            sed -i "${line}i $TAG\n" "$file"
        fi
    done
}

load_config()
{
    oldIFS=$IFS IFS='|'
    for file in $1; do
        [[ -f $file ]] && source "$file"
    done
    IFS=$oldIFS
}

__touch()
{
    for item in "$@"; do
        [[ -f $item ]] || touch "$item"
    done
}

__mkdir()
{
    for item in "$@"; do
        [[ -d $item ]] || mkdir --parents "$item"
    done
}

backup()
{
    mv --force $1 ${1}-backup-$(date +%Y-%m-%d-%H-%M)
}

is_backup()
{
    if [[ -f $2 ]] && ! diff $1 $2 &>/dev/null; then
        backup $2
    fi
}

copy_with_backup()
{
    [[ $# == 2 ]] || {
        msg "copy_with_backup() needs 2 arguments: source(1) and destination(2)"
        return 1
    }

    local src=$1
    local dst=$2

    if [[ -d $src ]]; then
        [[ -d $dst ]] || mkdir --parents $dst
        src="$src/*"
    fi

    for item in $src; do
        local name=${item##*/}       # ${1##*/} == file or directory name
        if [[ -d $item ]]; then
            copy_with_backup "$item" "$dst/$name"
        elif [[ -f $item ]]; then
            is_backup $item $dst/$name
            cp --no-clobber "$item" "$dst"
        fi
    done
}


#################
###   Debug   ###
#################

debug_var()
{
    msg "DEBUG:"
    for var in $*; do
        printf "\t"
        eval echo >&2 "$var = \${$var}"
    done
}

debug_arg()
{
    msg ${ORANGE}"sum arg: $#"${NOFORMAT}
    for i in $@; do
        printf ${BLUE}"\targ: $i\n"${NOFORMAT}
    done
}
